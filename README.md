Foreach Common Java libraries
=============================

The Foreach Java Common Libraries is a set of different Java libraries (JAR) files that contain
a number of useful, reusable classes (like utilities or base classes).  These are used in different projects
and are considered production ready.

Different modules include utilities for testing, object locking, distributed locking and type conversion.

More information:

* Documentation and release notes: https://foreach.atlassian.net/wiki/display/FJCL/Common+Java+libraries+Home
* Issue tracker: https://foreach.atlassian.net/projects/FJCL
* Source code: https://bitbucket.org/beforeach/common-java-libraries

Developed and maintained by [Foreach.be](http://www.foreach.be)